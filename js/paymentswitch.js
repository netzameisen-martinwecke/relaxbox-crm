/**
 * Payment Switch for the Upgrade view
 * Depends on jquery
 */
var paymentSwitch = ( function() {
	var settings = {
		selector: {
			label: '.paymentswitch-label',
			input: '.paymentswitch-input',
			options: {
				monthly: '.comparisontable-option[data-option="monthly-payment"]',
				yearly: '.comparisontable-option[data-option="yearly-payment"]'
			},
			comparisonTable: {
				table: '.comparisontable'
			}
		},
		options: [
			'monthly',
			'yearly'
		],	
		state: false
	}

	var init = function() {
		$( 'html' )
			.addClass( 'paymentswitch--initiated' );

		settings.state = $( settings.selector.input ).prop( 'checked' );
		onChange();

		bindEventhandlers();
	}

	var bindEventhandlers = function() {
		$( document )
			.on( 'change', settings.selector.input, function( event ) {
				settings.state = $( this ).prop( 'checked' );

				$( document ).trigger( 'paymentSwitch/change', [{ state: settings.state }] );
			} ).
			on( 'paymentSwitch/change', function( event, data ) {
				onChange();
			} );
	}

	var onChange = function() {
		var option = ( settings.state ) ? settings.options[1] : settings.options[0];

		$( settings.selector.comparisonTable.table )
			.attr( 'data-payment', option );
	}

	return {
		init: function() { init(); }
	}
} )();


$( document ).ready( function() {
	paymentSwitch.init();	
} );